﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomeSize
{
    class Ruler
    {
        private int id; //番号
        private string name;//名前
        private int sizeNum;//値
        private string unit;//単位
        private string imgSrc;

        public Ruler(int id, string name, int sizeNum, string unit, string imgSrc)
        {
            this.id = id;
            this.name = name;
            this.sizeNum = sizeNum;
            this.unit = unit;
            this.imgSrc = imgSrc;
        }

        public string Unit { get => unit; set => unit = value; }
        public int SizeNum { get => sizeNum; set => sizeNum = value; }
        public string Name { get => name; set => name = value; }
        public int Id { get => id; set => id = value; }
        public string ImgSrc { get => imgSrc; set => imgSrc = value; }
    }
}
